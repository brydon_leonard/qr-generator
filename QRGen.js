$(document).ready(function(){
	var arr = [];
	for (var i = 0; i < 100; i++)
	{
		var tempArr = [];
		for (var j = 0; j < 100; j++)
		{
			if (Math.random() > 0.5)
				tempArr.push(false);
			else tempArr.push(true);
		}
		arr.push(tempArr);
	}
	var data = '1234567890';
	DrawArray(arr);
	$('body').append($('<h5>'+GenQR("HELLO WORLD") + '</h5>'));
});

//Require square grid
DrawArray = function(grid)
{	
	var c = $('#canvas');
	var ctx = c[0].getContext("2d");
	
	ctx.fillStyle = '#000000';
	
	var x = grid.length;
	var y = grid[0].length;
	
	var blockSize = {width:500/x, height:500/y};
	
	for (var i = 0; i < x; i++)
	{
		for (var j = 0; j < y; j++)
		{
			if (grid[i][j])
				ctx.fillRect(blockSize.width * i, blockSize.height * j, blockSize.width, blockSize.height);
		}
	}
}

GenQR = function(data)
{
	var isNumeric = true;
	for (var i = 0; i < data.length; i++)
	{
		if (!(data[i] >= '0' && data[i] <= '9')){
			isNumeric = false;
			break;
		}		
	}
	
	var modeIndicator;
	if (isNumeric)
		modeIndicator = '0001';
	else modeIndicator = '0010';
	
	var charCountIndicator = data.length.toString(2);
	if (isNumeric)
		while (charCountIndicator.length < 10)
			charCountIndicator = '0' + charCountIndicator;
	else while (charCountIndicator.length < 9)
		charCountIndicator = '0' + charCountIndicator;
	
	var encodedData
	if (isNumeric)
		encodedData = EncodeData('numeric', data);
	else encodedData = EncodeData('alphanumeric', data);
	
	var fullEncode = '';
	fullEncode+= modeIndicator;
	fullEncode+=charCountIndicator;
	for (var i = 0; i < encodedData.length; i++)
	{
		fullEncode+=encodedData[i];
	}
	fullEncode+='0000';
	while (fullEncode.length % 8 != 0)	
		fullEncode += '0';
	i = 0;
	while(fullEncode.length < 152)
	{
		if (i == 0){
			fullEncode += '11101100'
			i = 1;
		}else{
			fullEncode += '00010001';
			i = 0;
		}
		
	}
	return fullEncode;	
}

EncodeData = function(encodeType, data)
{	
	var charRefernces = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',' ','$','%','*','+','-','.','/',':'];
	var encodedData = [];
	if (encodeType === "numeric"){
		var i = 0;
		encodedData.push('');
		while (data.length > 0)
		{
			if (encodedData[i].length >= 3){
				encodedData[i] = (encodedData[i]*1.0).toString(2);
				i++;
				encodedData.push('');
			}
			
			encodedData[i] = encodedData[i] + data[0];
			data = data.slice(1, data.length);
		}
		encodedData[i] = (encodedData[i]*1.0).toString(2);
	}else{
		var i = 0;
		var j = 0;
		encodedData.push('');
		while (data.length > 0)
		{
			if (j == 2){
				encodedData[i] = (encodedData[i]*1.0).toString(2);
				while (encodedData[i].length < 11)
					encodedData[i] = '0' + encodedData[i];
				i++
				j = 0;
				encodedData.push('');
			}
			
			var val = charRefernces.indexOf(data[0]);	
			data = data.slice(1, data.length);
			if (val > -1)
				encodedData[i] = encodedData[i] + val;
			if (j ==0 && data.length > 0)
				encodedData[i] *= 45;
			j++;
		}
		encodedData[i] = (encodedData[i]*1.0).toString(2);
		if (encodedData[i].length == 1){
			while (encodedData[i].length < 6)
			encodedData[i] = '0' + encodedData[i];
		}else
			while (encodedData[i].length < 11)
			encodedData[i] = '0' + encodedData[i];
	}	
	return encodedData;	
}